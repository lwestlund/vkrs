use super::queue_family_indices::QueueFamilyIndices;
use super::validation;
use super::vulkan;

use ash::vk;

pub struct VkContext {
    _entry: ash::Entry,
    instance: ash::Instance,
    debug_utils_loader: ash::extensions::ext::DebugUtils,
    debug_messenger: vk::DebugUtilsMessengerEXT,
    surface: ash::extensions::khr::Surface,
    surface_khr: vk::SurfaceKHR,
    physical_device: vk::PhysicalDevice,
    queue_family_indices: QueueFamilyIndices,
    device: ash::Device,
    graphics_queue: vk::Queue,
    present_queue: vk::Queue,
    memory_properties: vk::PhysicalDeviceMemoryProperties,
}

impl VkContext {
    pub fn instance(&self) -> &ash::Instance {
        &self.instance
    }

    pub fn surface(&self) -> &ash::extensions::khr::Surface {
        &self.surface
    }

    pub fn surface_khr(&self) -> vk::SurfaceKHR {
        self.surface_khr
    }

    pub fn physical_device(&self) -> vk::PhysicalDevice {
        self.physical_device
    }

    pub fn queue_family_indices(&self) -> &QueueFamilyIndices {
        &self.queue_family_indices
    }

    pub fn device(&self) -> &ash::Device {
        &self.device
    }

    pub fn graphics_queue(&self) -> vk::Queue {
        self.graphics_queue
    }

    pub fn present_queue(&self) -> vk::Queue {
        self.present_queue
    }

    pub fn memory_properties(&self) -> vk::PhysicalDeviceMemoryProperties {
        self.memory_properties
    }
}

impl VkContext {
    pub fn new(name: &'static str, version: u32, window: &winit::window::Window) -> Self {
        let entry = unsafe { ash::Entry::load().expect("Failed to load Vulkan.") };

        let instance = vulkan::create_instance(name, version, &entry, window);
        let (debug_utils_loader, debug_messenger) =
            vulkan::setup_debug_messenger(&entry, &instance);

        let surface = ash::extensions::khr::Surface::new(&entry, &instance);
        let surface_khr = unsafe {
            ash_window::create_surface(&entry, &instance, window, None)
                .expect("Failed to create surface")
        };

        let (physical_device, queue_family_indices) =
            vulkan::select_physical_device(&instance, &surface, surface_khr);

        let (device, graphics_queue, present_queue) =
            vulkan::create_logical_device_with_graphics_and_present_queue(
                &instance,
                &queue_family_indices,
                physical_device,
            );

        let memory_properties =
            unsafe { instance.get_physical_device_memory_properties(physical_device) };

        Self {
            _entry: entry,
            instance,
            debug_utils_loader,
            surface,
            surface_khr,
            debug_messenger,
            physical_device,
            queue_family_indices,
            device,
            graphics_queue,
            present_queue,
            memory_properties,
        }
    }
}

impl VkContext {
    pub fn wait_gpu_idle(&self) {
        unsafe { self.device.device_wait_idle().unwrap() };
    }

    pub fn destroy(&self) {
        unsafe {
            self.device.destroy_device(None);
            self.surface.destroy_surface(self.surface_khr, None);
            if validation::ENABLE_VALIDATION_LAYERS {
                self.debug_utils_loader
                    .destroy_debug_utils_messenger(self.debug_messenger, None);
            }
            self.instance.destroy_instance(None);
        }
    }
}
